﻿/////////////////////////////////////////////////////////////////////////////////
//
//	ControllerScript.cs
//	Unity Multi-Controller HID Example
//	© 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Tutorial Sample Project that demonstrates HID Controller 
//					Implementation in Unity
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour 
{
	private int[] 	tableRowsYPos;
	private int 	rightTableXPos,
					leftTableXPos,
					tableWidth,
					rowHeight;
	
	void OnGUI() 
	{
		// Our KeyCodes and Axis are using Unitys default mapping scheme for iOS Controllers 

		// http://blogs.unity3d.com/2013/10/11/unity-4-2-2-brings-ios-game-controller-support/

		// Get Control Count.
		string[] controllers = Input.GetJoystickNames();

		// If One Controller is Connected
		if (controllers.Length > 0)
		{
			// Player One Controls
			GUI.color = Color.white;
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Currently Connected : " 		+ Input.GetJoystickNames()[0]				);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick1Button0)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick1Button1)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick1Button2)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick1Button3)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick1Button4)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick1Button5)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "L3 Button : "     			+ Input.GetKey (KeyCode.Joystick1Button8)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R3 Button : "     			+ Input.GetKey (KeyCode.Joystick1Button9)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "Start Button : "     			+ Input.GetKey (KeyCode.Joystick1Button10)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "R2 Axis : " 					+ Input.GetAxis ("Joy1R2")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "L2 Axis : "     				+ Input.GetAxis ("Joy1L2")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Up/Down Axis : "    		+ Input.GetAxis ("Joy1UpDown")				);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Left/Right Axis : "  	+ Input.GetAxis ("Joy1LeftRight")			);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy1X")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy1Y")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy1Z")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy1RZ")					);
		}
		else
		{
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Controller One Disconnected");
		}

		// If a Second Controller is available
		if (controllers.Length > 1)
		{
			// Player Two Controls
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Currently Connected : " 		+ Input.GetJoystickNames()[0]				);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick2Button0)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick2Button1)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick2Button2)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick2Button3)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick2Button4)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick2Button5)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "L3 Button : "     			+ Input.GetKey (KeyCode.Joystick2Button8)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R3 Button : "     			+ Input.GetKey (KeyCode.Joystick2Button9)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "Start Button : "     		+ Input.GetKey (KeyCode.Joystick2Button10)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "R2 Axis : " 					+ Input.GetAxis ("Joy2R2")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "L2 Axis : "     				+ Input.GetAxis ("Joy2L2")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Up/Down Axis : "    	+ Input.GetAxis ("Joy2UpDown")				);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Left/Right Axis : "  	+ Input.GetAxis ("Joy2LeftRight")			);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy2X")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy2Y")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy2Z")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy2RZ")					);
		}
		else
		{
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[0],  tableWidth, 60), 		"Controller Two Disconnected");

		}
	}
	
	void Awake() 
	{
		rowHeight 			= 20;
		int rowSpacing 		= 5;
		tableRowsYPos 		= new int[18];
		tableRowsYPos[0] 	= 10;
		rightTableXPos 		= Screen.width/2;
		leftTableXPos 		= 0;
		tableWidth 			= Screen.width/2;

		for (int i = 1; i < 18; i++)
		{
			tableRowsYPos[i] 	= tableRowsYPos[i-1]  + rowHeight + rowSpacing;
		}
	}
}

