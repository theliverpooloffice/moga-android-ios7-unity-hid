﻿/////////////////////////////////////////////////////////////////////////////////
//
//	ControllerScript.cs
//	Unity Multi-Controller HID Example
//	© 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Tutorial Sample Project that demonstrates HID Controller 
//					Implementation in Unity for Android and iOS Controllers
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour 
{
	private int[] 	tableRowsYPos;
	private int 	rightTableXPos,
					leftTableXPos,
					tableWidth,
					rowHeight;

	string[] controllerNames;

	void OnGUI() 
	{
		// Get Control Count.
		controllerNames = Input.GetJoystickNames();

		// If One Controller is Connected
		if (controllerNames.Length > 0)
		{
			// Player One Controls
			GUI.color = Color.white;
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Currently Connected : " 		+ controllerNames[0]						);

			#if UNITY_IPHONE
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick1Button14)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick1Button13)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick1Button15)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick1Button12)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick1Button8)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick1Button9)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "Pause Button : "     			+ Input.GetKey (KeyCode.Joystick1Button0)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R2 Button : " 				+ Input.GetKey (KeyCode.Joystick1Button11)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "L2 Button : "     			+ Input.GetKey (KeyCode.Joystick1Button10)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "DPad Up Button : "    		+ Input.GetKey (KeyCode.Joystick1Button4)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "DPad Down Button : "    		+ Input.GetKey (KeyCode.Joystick1Button6)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Left Button : "  		+ Input.GetKey (KeyCode.Joystick1Button7)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Right Button : "    		+ Input.GetKey (KeyCode.Joystick1Button5)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy1X")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy1Y")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy1Z")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy1RZ")					);
			#endif

			#if UNITY_ANDROID || UNITY_WP8
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick1Button0)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick1Button1)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick1Button2)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick1Button3)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick1Button4)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick1Button5)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "L3 Button : "     			+ Input.GetKey (KeyCode.Joystick1Button8)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R3 Button : "     			+ Input.GetKey (KeyCode.Joystick1Button9)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "Start Button : "     			+ Input.GetKey (KeyCode.Joystick1Button10)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "R2 Axis : " 					+ Input.GetAxis ("Joy1R2")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "L2 Axis : "     				+ Input.GetAxis ("Joy1L2")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Up/Down Axis : "    		+ Input.GetAxis ("Joy1UpDown")				);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Left/Right Axis : "  	+ Input.GetAxis ("Joy1LeftRight")			);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy1X")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy1Y")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy1Z")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy1RZ")					);
			#endif
		}
		else
		{
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Controller One Disconnected");
		}

		// If a Second Controller is available
		if (controllerNames.Length > 1)
		{
			// Player Two Controls
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Currently Connected : " 		+ controllerNames[1]				);
			
			#if UNITY_IPHONE
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick2Button14)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick2Button13)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick2Button15)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick2Button12)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick2Button8)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick2Button9)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "Pause Button : "     		+ Input.GetKey (KeyCode.Joystick2Button0)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R2 Button : " 				+ Input.GetKey (KeyCode.Joystick2Button11)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "L2 Button : "     			+ Input.GetKey (KeyCode.Joystick2Button10)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "DPad Up Button : "    		+ Input.GetKey (KeyCode.Joystick2Button4)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "DPad Down Button : "    		+ Input.GetKey (KeyCode.Joystick2Button6)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Left Button : "  		+ Input.GetKey (KeyCode.Joystick2Button7)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Right Button : "    	+ Input.GetKey (KeyCode.Joystick2Button5)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy2X")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy2Y")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy2Z")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy2RZ")					);
			#endif
			
			#if UNITY_ANDROID || UNITY_WP8
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ Input.GetKey (KeyCode.Joystick2Button0)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ Input.GetKey (KeyCode.Joystick2Button1)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ Input.GetKey (KeyCode.Joystick2Button2)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ Input.GetKey (KeyCode.Joystick2Button3)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ Input.GetKey (KeyCode.Joystick2Button4)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ Input.GetKey (KeyCode.Joystick2Button5)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[7],  tableWidth, rowHeight), "L3 Button : "     			+ Input.GetKey (KeyCode.Joystick2Button8)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R3 Button : "     			+ Input.GetKey (KeyCode.Joystick2Button9)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "Start Button : "     		+ Input.GetKey (KeyCode.Joystick2Button10)	);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "R2 Axis : " 					+ Input.GetAxis ("Joy2R2")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "L2 Axis : "     				+ Input.GetAxis ("Joy2L2")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Up/Down Axis : "    	+ Input.GetAxis ("Joy2UpDown")				);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Left/Right Axis : "  	+ Input.GetAxis ("Joy2LeftRight")			);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Joy2X")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Joy2Y")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[16], tableWidth, rowHeight), "Right Stick Horizontal : "	+ Input.GetAxis ("Joy2Z")					);
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[17], tableWidth, rowHeight), "Right Stick Vertical : "   	+ Input.GetAxis ("Joy2RZ")					);
			#endif
		}
		else
		{
			GUI.Label(new Rect(rightTableXPos, tableRowsYPos[0],  tableWidth, 60), 		"Controller Two Disconnected");
		}
	}
	
	void Awake() 
	{
		rowHeight 			= 20;
		int rowSpacing 		= 5;
		tableRowsYPos 		= new int[18];
		tableRowsYPos[0] 	= 10;
		rightTableXPos 		= Screen.width/2;
		leftTableXPos 		= 0;
		tableWidth 			= Screen.width/2;

		for (int i = 1; i < 18; i++)
		{
			tableRowsYPos[i] 	= tableRowsYPos[i-1]  + rowHeight + rowSpacing;
		}
	}
}

