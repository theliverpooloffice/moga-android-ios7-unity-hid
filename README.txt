MOGA Android iOS7 Unity HID - Sample Project

 Features:
  - MOGA - Unity Android iOS7 HID: Android and iOS MultiController Demo
  - MOGA Android Unity Input Manager Config: Preset InputManager Configuration
  - iOS Analogue Button Fix: Workaround to Poll Analogue Button Values

Copyright � 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
