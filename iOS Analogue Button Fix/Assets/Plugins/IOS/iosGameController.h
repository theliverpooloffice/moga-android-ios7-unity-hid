#import <Foundation/Foundation.h>
#import <GameController/GameController.h>

@interface ControllerPlugin : NSObject
{
}

+ (void)load;
+ (void)connectController:(NSNotification *)notification;

@end

GCController * controller;
int conStatus;
int paused;