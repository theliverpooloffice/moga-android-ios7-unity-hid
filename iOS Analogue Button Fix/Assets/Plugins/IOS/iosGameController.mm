
#import "iosGameController.h"
#import "UnityAppController.h"
#import <GameController/GameController.h>

@implementation ControllerPlugin

+ (void)load
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectController:)
                                                 name:UIApplicationDidFinishLaunchingNotification object:nil];
}

// Connect --------------------------------------------------------------------------------------------------------------

+ (void)connectController:(NSNotification *)notification
{
    // Receive notifications when a controller connects or disconnects.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameControllerDidConnect:) name:GCControllerDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameControllerDidDisconnect:) name:GCControllerDidDisconnectNotification object:nil];
}

// Connected -------------------------------------------------------------------------------------------------------------

+ (void)gameControllerDidConnect:(NSNotification *)notification
{
    conStatus = 1;
    controller = notification.object;
    controller.playerIndex = 0;
}

// Disconnected ----------------------------------------------------------------------------------------------------------

+ (void)gameControllerDidDisconnect:(NSNotification *)notification
{
    conStatus = 0;
    controller = notification.object;
}

// Unity View Controller  ------------------------------------------------------------------------------------------------

UIViewController * UnityGetGLViewController();

@end

// Unity Calls  ----------------------------------------------------------------------------------------------------------

extern "C" {
	
    typedef struct
    {
        float keyCode_A;
        float keyCode_B;
        float keyCode_X;
        float keyCode_Y;
        float keyCode_L1;
        float keyCode_R1;
        float keyCode_DpadUp;
        float keyCode_DpadDown;
        float keyCode_DpadLeft;
        float keyCode_DpadRight;
        float keyCode_L2;
        float keyCode_R2;
        
    } MogaStateStruct;
    
    // GetControllerState  ---------------------------------------------------------------------------------------------------
    
    MogaStateStruct _GetControllerState()
    {
        MogaStateStruct outState;
        
        if (conStatus == 1)
        {
            outState.keyCode_A           = controller.gamepad.buttonA.value;
            outState.keyCode_B           = controller.gamepad.buttonB.value;
            outState.keyCode_X           = controller.gamepad.buttonX.value;
            outState.keyCode_Y           = controller.gamepad.buttonY.value;
            outState.keyCode_L1          = controller.gamepad.leftShoulder.value;
            outState.keyCode_R1          = controller.gamepad.rightShoulder.value;
            outState.keyCode_DpadUp      = controller.gamepad.dpad.up.value;
            outState.keyCode_DpadDown    = controller.gamepad.dpad.down.value;
            outState.keyCode_DpadLeft    = controller.gamepad.dpad.left.value;
            outState.keyCode_DpadRight   = controller.gamepad.dpad.right.value;
            outState.keyCode_L2          = controller.extendedGamepad.leftTrigger.value;
            outState.keyCode_R2          = controller.extendedGamepad.rightTrigger.value;

        }
        
        return outState;
    }
}

