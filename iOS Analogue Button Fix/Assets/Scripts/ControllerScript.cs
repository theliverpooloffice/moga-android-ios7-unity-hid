﻿/////////////////////////////////////////////////////////////////////////////////
//
//	ControllerScript.cs
//	Unity iOS Analogue Controller Example
//	© 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Tutorial Sample Project that demonstrates how to use
//					the Analogue Buttons on an iOS Controller.
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour 
{
	private int[] 				tableRowsYPos;
	private int 				leftTableXPos,
								tableWidth,
								rowHeight;

	string[] 					controllerNames;

	private iMogaController 	iMogaController;

	void Awake()
	{
		iMogaController = iMogaController.createController();
	}

	void OnGUI() 
	{
		// Get Control Count.
		controllerNames = Input.GetJoystickNames();

		// If One Controller is Connected
		if (controllerNames.Length == 1)
		{
			// Poll the Moga Controller Analogue Buttons
			iMogaController.Update();

			// Player One Controls
			GUI.color = Color.white;
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Currently Connected : " 		+ controllerNames[0]						);

			#if UNITY_IPHONE
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[1],  tableWidth, rowHeight), "A Button : "  				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button14)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[2],  tableWidth, rowHeight), "B Button : "  				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button13)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[3],  tableWidth, rowHeight), "X Button : "  				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button15)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[4],  tableWidth, rowHeight), "Y Button : "  				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button12)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[5],  tableWidth, rowHeight), "L1 Button : " 			   	+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button8)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[6],  tableWidth, rowHeight), "R1 Button : " 				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button9)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[8],  tableWidth, rowHeight), "R2 Button : " 				+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button11)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[9],  tableWidth, rowHeight), "L2 Button : "     			+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button10)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[10], tableWidth, rowHeight), "DPad Up Button : "    		+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button4)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[11], tableWidth, rowHeight), "DPad Down Button : "    		+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button6)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[12], tableWidth, rowHeight), "DPad Left Button : "  		+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button7)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[13], tableWidth, rowHeight), "DPad Right Button : "    		+ iMogaController.getAnalogueButton(KeyCode.Joystick1Button5)	);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[14], tableWidth, rowHeight), "Left Stick Horizontal : " 	+ Input.GetAxis ("Horizontal")					);
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[15], tableWidth, rowHeight), "Left Stick Vertical : " 		+ Input.GetAxis ("Vertical")					);
			#endif
		}
		else
		{
			GUI.Label(new Rect(leftTableXPos, tableRowsYPos[0],  tableWidth, 60), 		 "Controller One Disconnected");
		}
	}
	
	void Start() 
	{
		rowHeight 			= 20;
		int rowSpacing 		= 5;
		tableRowsYPos 		= new int[18];
		tableRowsYPos[0] 	= 10;
		leftTableXPos 		= 0;
		tableWidth 			= Screen.width/2;

		for (int i = 1; i < 18; i++)
		{
			tableRowsYPos[i] 	= tableRowsYPos[i-1]  + rowHeight + rowSpacing;
		}
	}
}

