/////////////////////////////////////////////////////////////////////////////////
//
//	iMoga_Controller.cs
//	Unity iOS MOGA Analogue Fix
//	© 2014 Bensussen Deutsch and Associates, Inc. All rights reserved.
//
//	description:	Enables iOS Analogue Buttons in Unity.
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System;
using System.Collections.Generic;


using System.Runtime.InteropServices;

public class iMogaController
{
	public  static 		MogaState currentState;
	private static 		iMogaController mController;
	
	public struct MogaState 
	{
		public float keyCode_A;
    	public float keyCode_B;
    	public float keyCode_X;
    	public float keyCode_Y;
    	public float keyCode_L1;
    	public float keyCode_R1;
		public float keyCode_DpadUp;
		public float keyCode_DpadDown;
		public float keyCode_DpadLeft;
		public float keyCode_DpadRight;
		public float keyCode_L2;
		public float keyCode_R2;
	}

	// ------------------------------------------------------------------------------------------------
	private iMogaController() 
	{
	}
	
	// ------------------------------------------------------------------------------------------------
	public static iMogaController createController()
	{
		if (mController == null)
		{
			mController = new iMogaController();
		}
		return mController;
	}
	
	// Polls the Controller State ---------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------
	public void Update()
	{
		#if UNITY_IPHONE
		currentState = getControllerState();
		#endif
	}
	
	// Retrieve the value of the requested key code ---------------------------------------------------
	// ------------------------------------------------------------------------------------------------
	public static float getAnalogueButton(KeyCode keyCode)
	{
		#if UNITY_IPHONE
		switch(keyCode) 
		{
			case KeyCode.Joystick1Button14:    	 	return currentState.keyCode_A;
			case KeyCode.Joystick1Button13:		 	return currentState.keyCode_B;
			case KeyCode.Joystick1Button15:    		return currentState.keyCode_X;
			case KeyCode.Joystick1Button12:         return currentState.keyCode_Y;
			case KeyCode.Joystick1Button8:         	return currentState.keyCode_L1;
			case KeyCode.Joystick1Button9:        	return currentState.keyCode_R1;
			case KeyCode.Joystick1Button4:         	return currentState.keyCode_DpadUp;
			case KeyCode.Joystick1Button6:         	return currentState.keyCode_DpadDown;
			case KeyCode.Joystick1Button7:        	return currentState.keyCode_DpadLeft;
			case KeyCode.Joystick1Button5:        	return currentState.keyCode_DpadRight;
			case KeyCode.Joystick1Button10:        	return currentState.keyCode_L2;
			case KeyCode.Joystick1Button11:        	return currentState.keyCode_R2;
			default: return 0.0f;
		}
		#endif
		
		return 0.0f;
	}
	
	// ------------------------------------------------------------------------------------------------
	// Bindings
	// ------------------------------------------------------------------------------------------------
	[DllImport("__Internal")]
	private static extern MogaState _GetControllerState();

	public static MogaState getControllerState()
	{
		return _GetControllerState();
	}
}